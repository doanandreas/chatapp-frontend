import 'styled-components'

declare module 'styled-components' {
  export interface DefaultTheme {
    colors: {
      body: string
      primary: string
      lightGray: string
      gray: string
      grayChat: string
      greenChat: string
      greenButton: string
      border: string
    }
    fonts: {
      heading: string
      body: string
    }
  }
}
