import React, { ReactElement } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { UserProvider } from 'store/UserContext'
import { SocketProvider } from 'store/SocketContext'

import {
  LandingPage,
  LoginPage,
  ChatPage,
  JoinChatroomPage,
  CreateChatroomPage,
} from 'containers'
import ProtectedRoute from './ProtectedRoute'

const Routes = (): ReactElement => (
  <BrowserRouter>
    <UserProvider>
      <SocketProvider>
        <Switch>
          <Route path="/login/" component={LoginPage} />
          <ProtectedRoute exact path="/" component={LandingPage} />
          <ProtectedRoute path="/chat/:roomId/" component={ChatPage} />
          <ProtectedRoute path="/room/join/" component={JoinChatroomPage} />
          <ProtectedRoute path="/room/create/" component={CreateChatroomPage} />
        </Switch>
      </SocketProvider>
    </UserProvider>
  </BrowserRouter>
)

export default Routes
