import React, { useContext, ElementType, ReactElement } from 'react'
import styled from 'styled-components'
import { Route, Redirect } from 'react-router-dom'

import { Spinner } from 'components'
import { Center } from 'components/styled'
import { UserContext } from 'store/UserContext'

type ProtectedRouteProps = {
  component: ElementType
  path: string
  exact?: boolean
}

const LoadContainer = styled(Center)`
  height: 100vh;
`

const ProtectedRoute = ({
  component: Component,
  ...rest
}: ProtectedRouteProps): ReactElement => {
  const { isAuthenticated, userIsLoading } = useContext(UserContext)

  if (userIsLoading) {
    return (
      <LoadContainer>
        <Spinner size="45" color="white" />
      </LoadContainer>
    )
  }

  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuthenticated ? (
          <Component />
        ) : (
          <Redirect
            to={{
              pathname: '/login/',
              state: {
                referrer: location,
              },
            }}
          />
        )
      }
    />
  )
}

export default ProtectedRoute
