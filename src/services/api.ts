import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { Json } from 'types/general'

export async function request(
  url: string,
  payload: Json = {},
  method: AxiosRequestConfig['method'] = 'GET',
  headers: Record<string, string> = {},
): Promise<AxiosResponse> {
  const requestData: AxiosRequestConfig = {
    url,
    method,
    data: JSON.stringify(payload),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json;charset=UTF-8',
      ...headers,
    },
  }

  try {
    const response = await axios(requestData)
    return await response.data
  } catch (err) {
    throw err.response && err.response.data
  }
}
