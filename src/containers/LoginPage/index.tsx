import React, { useContext, ReactElement } from 'react'
import styled from 'styled-components'
import { Redirect } from 'react-router-dom'
import GoogleLogin from 'react-google-login'

import { Logo } from 'components'
import { Center, Column, WhiteBox, Heading } from 'components/styled'
import { UserContext } from 'store/UserContext'
import { GOOGLE_CLIENT_ID } from 'constants/auth'

type LoginPageProps = {
  location: {
    state?: {
      referrer: Location
    }
  }
}

const Layout = styled(Column)`
  justify-content: space-between;
  align-items: center;
  min-height: 100vh;
  padding: 7vh 10vw;
`

const WhiteHeading = styled(Heading)`
  color: white;
  margin-top: calc(0.3rem + 0.3vh);
`

const LoginText = styled(Heading)`
  font-size: calc(1.3rem + 0.5vw);
  color: ${({ theme }) => theme.colors.primary};
`

const LoginPage = ({ location }: LoginPageProps): ReactElement => {
  const { isAuthenticated, login } = useContext(UserContext)

  if (isAuthenticated && location.state) {
    return <Redirect to={location.state.referrer} />
  }

  return (
    <Layout>
      <Center>
        <Logo />
        <WhiteHeading>A Functional Chatting App</WhiteHeading>
        <WhiteHeading>Built with React.js and Phoenix Framework</WhiteHeading>
        <WhiteBox spacing="1rem" style={{ marginTop: '5vh' }}>
          <LoginText>Log in to your account</LoginText>
          <GoogleLogin
            clientId={GOOGLE_CLIENT_ID}
            buttonText="Sign in with Google"
            onSuccess={login}
            onFailure={console.log}
            cookiePolicy="single_host_origin"
          />
        </WhiteBox>
      </Center>
      <p style={{ color: 'white', textAlign: 'center' }}>
        Create with ❤ by Doan, Glenda, Kefas, and Wulan
      </p>
    </Layout>
  )
}

export default LoginPage
