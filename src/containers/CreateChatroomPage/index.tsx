import React, { FormEvent, ReactElement } from 'react'
import styled from 'styled-components'
import { useForm, FormProvider } from 'react-hook-form'

import { Button, GreenButton, Layout, Spinner } from 'components'
import { CreateChatroomForm } from 'components/Form'
import {
  WhiteBox,
  SpacedRow,
  Heading,
  Center,
  SpacedColumn,
} from 'components/styled'
import { useChatRoom } from 'hooks'

const Container = styled(Center)`
  height: calc(80vh - 3.5rem);
`

const Title = styled(Heading)`
  font-size: calc(1rem + 0.2vw);
  color: ${({ theme }) => theme.colors.primary};
`

const CreateChatroomPage = (): ReactElement => {
  const methods = useForm()
  const { formState, getValues, setError, handleSubmit, reset } = methods

  const { createChatRoom } = useChatRoom(setError)

  const onSubmit = () => {
    createChatRoom(getValues(['id', 'name']))
  }

  const cancel = (e: FormEvent) => {
    e.preventDefault()
    reset({ id: '', name: '' })
  }

  return (
    <Layout>
      <Container>
        <WhiteBox spacing="1.5rem">
          <Title>Create a new Chatroom</Title>
          <form style={{ width: '100%' }}>
            <SpacedColumn spacing="0.8rem">
              <FormProvider {...methods}>
                <CreateChatroomForm />
              </FormProvider>
              <SpacedRow align="center">
                <GreenButton onClick={handleSubmit(onSubmit)}>
                  Create
                </GreenButton>
                <Button onClick={cancel}>Cancel</Button>
                {formState.isSubmitting && <Spinner />}
              </SpacedRow>
            </SpacedColumn>
          </form>
        </WhiteBox>
      </Container>
    </Layout>
  )
}

export default CreateChatroomPage
