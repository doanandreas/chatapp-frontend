import React, { ReactElement } from 'react'

import { Layout } from 'components'
import { Sidebar } from 'containers'
import { useWindowSize } from 'hooks'

const LandingPage = (): ReactElement => {
  const { isMobile } = useWindowSize()

  return <Layout>{isMobile ? <Sidebar /> : <></>}</Layout>
}

export default LandingPage
