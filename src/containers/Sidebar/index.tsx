import React, {
  ReactElement,
  useState,
  useCallback,
  useEffect,
  useContext,
} from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { EmptyPlaceholderIcon } from 'assets'
import { Heading, Column, Center } from 'components/styled'
import { Button, Spinner } from 'components'
import { request } from 'services/api'
import { CHAT_ROOM_BY_USER_ID_URL } from 'constants/urls'
import RoomList from 'containers/RoomList'
import { UserContext } from 'store/UserContext'

const LeftContainer = styled.div`
  width: 30%;
  height: calc(100vh - 3rem);

  @media (max-width: 768px) {
    width: 42%;
  }

  @media (max-width: 600px) {
    width: 100%;
  }
`

const SpinnerContainer = styled(LeftContainer)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const EmptyPlaceholderContainer = styled(Center)`
  margin-top: 155px;
  text-align: center;
  width: 100%;

  .img-placeholder {
    margin-bottom: 30px;
  }

  .heading-placeholder {
    font-size: 22px;
  }

  .div-placeholder {
    width: 185px;
  }

  .para-placeholder {
    margin-top: 8px;
    font-family: ${({ theme }) => theme.fonts.heading};
    font-size: 16px;
    font-weight: 400;
  }

  .button-col-placeholder {
    margin-top: 16px;

    > * {
      width: 100%;
    }

    > * > * {
      margin-top: 7px;
      border: 1px solid ${({ theme }) => theme.colors.border};
      padding: 12px 14px;
    }
  }
`

const Sidebar = (): ReactElement => {
  const { user } = useContext(UserContext)

  const [rooms, setRooms] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  const fetchRoomData = useCallback(async () => {
    await request(CHAT_ROOM_BY_USER_ID_URL(user.id))
      .then(({ data }) => setRooms(data))
      .catch(console.error)
    setIsLoading(false)
  }, [user.id])

  useEffect(() => {
    fetchRoomData()
  }, [fetchRoomData])

  const EmptyPlaceholder = (
    <EmptyPlaceholderContainer>
      <div className="div-placeholder">
        <EmptyPlaceholderIcon className="img-placeholder" />
        <Heading bolder className="heading-placeholder">
          No chatrooms yet
        </Heading>
        <p className="para-placeholder">
          To get started,
          <br /> join or create a chatroom
        </p>
        <Column className="button-col-placeholder">
          <Link to="/room/join">
            <Button bgColor="#ffffff" fullWidth>
              Join existing chatroom
            </Button>
          </Link>
          <Link to="/room/create">
            <Button bgColor="#ffffff" fullWidth>
              Create new chatroom
            </Button>
          </Link>
        </Column>
      </div>
    </EmptyPlaceholderContainer>
  )

  if (isLoading) {
    return (
      <SpinnerContainer>
        <Spinner />
      </SpinnerContainer>
    )
  }
  return (
    <LeftContainer>
      <Column>
        {rooms.length > 0 ? <RoomList rooms={rooms} /> : EmptyPlaceholder}
      </Column>
    </LeftContainer>
  )
}

export default Sidebar
