import React, {
  FormEvent,
  ReactElement,
  useContext,
  useEffect,
  useState,
} from 'react'
import { useParams, Link } from 'react-router-dom'
import styled from 'styled-components'

import { BackIcon } from 'assets'
import { Layout, ChatMessage } from 'components'
import {
  SpacedColumn,
  Scrollbar,
  SpacedRow,
  Avatar,
  Column,
} from 'components/styled'
import { NEW_MESSAGE } from 'constants/action'
import { useChatChannel, useWindowSize } from 'hooks'
import { UserContext } from 'store/UserContext'
import { MessageResponse } from 'types/socket'
import { CHAT_ROOM_BY_ID_URL } from 'constants/urls'
import { request } from 'services/api'

type ChatRoom = {
  roomId: string
}

const ChatRoomInfo = styled(SpacedRow)`
  align-items: center;
  height: 3.5rem;
  padding: 0 1rem;
`

const ChatContainer = styled.div`
  display: flex;
  flex-direction: column-reverse;
  height: calc(100vh - 10.5rem);
  overflow: scroll;
  -webkit-overflow-scrolling: touch;
  padding: calc(0.2rem + 0.5vw);
  ${Scrollbar}
`

const DateText = styled.div`
  background-color: ${({ theme }) => theme.colors.grayChat};
  color: ${({ theme }) => theme.colors.primary};
  border-radius: 0.5rem;
  padding: 0.3rem 0.5rem;
  font-size: 0.9rem;
  margin-top: 1rem;
`

const ChatInput = styled.input`
  border: 0;
  border-top: 1px solid ${({ theme }) => theme.colors.border};
  width: 100%;
  padding: 1.2rem 1rem;

  ::placeholder
    color: ${({ theme }) => theme.colors.lightGray};
  }
`

const initialRoom = {
  name: 'Room',
  image_url: '',
  users_joined: 0,
}

const ChatPage = (): ReactElement => {
  const { user } = useContext(UserContext)
  const { roomId } = useParams<ChatRoom>()
  const { isMobile } = useWindowSize()
  const { messages, broadcast } = useChatChannel(`room:${roomId}`)

  const [msg, setMsg] = useState('')
  const [chatRoom, setChatRoom] = useState(initialRoom)

  useEffect(() => {
    request(CHAT_ROOM_BY_ID_URL(roomId))
      .then(({ data }) => setChatRoom(data))
      .catch(console.error)
  }, [roomId])

  const sendMessage = (event: FormEvent) => {
    event.preventDefault()
    if (msg.trim() === '') return

    setMsg('')
    broadcast(NEW_MESSAGE, {
      sender: user.id,
      chat_room: roomId,
      message: msg,
    })
  }

  return (
    <Layout>
      <ChatRoomInfo>
        {isMobile && (
          <Link to="/">
            <BackIcon />
          </Link>
        )}
        <Avatar src={chatRoom.image_url} size="2.4rem" />
        <p style={{ fontSize: '0.9rem' }}>
          {chatRoom.name} ({chatRoom.users_joined})
        </p>
      </ChatRoomInfo>
      <ChatContainer>
        <Column>
          {Object.entries(messages).map(([date, messageList]) => (
            <SpacedColumn key={`messages-${date}`} spacing="1rem">
              <Column align="center">
                <DateText>{date}</DateText>
              </Column>
              {messageList.map((msg: MessageResponse, id) => (
                <ChatMessage key={id} {...msg} />
              ))}
            </SpacedColumn>
          ))}
        </Column>
      </ChatContainer>
      <form onSubmit={sendMessage}>
        <ChatInput
          value={msg}
          onChange={e => setMsg(e.target.value)}
          placeholder="Enter a message..."
        />
      </form>
    </Layout>
  )
}

export default ChatPage
