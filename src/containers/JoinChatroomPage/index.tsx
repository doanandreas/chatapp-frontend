import React, { FormEvent, ReactElement } from 'react'
import styled from 'styled-components'
import { useForm, FormProvider } from 'react-hook-form'

import { Button, GreenButton, Layout, Spinner } from 'components'
import { JoinChatroomForm } from 'components/Form'
import {
  WhiteBox,
  SpacedRow,
  Heading,
  Center,
  SpacedColumn,
} from 'components/styled'
import { useChatRoom } from 'hooks'

const Container = styled(Center)`
  height: calc(80vh - 3.5rem);
`

const Title = styled(Heading)`
  font-size: calc(1rem + 0.2vw);
  color: ${({ theme }) => theme.colors.primary};
`

const JoinChatroomPage = (): ReactElement => {
  const methods = useForm()
  const { formState, getValues, setError, handleSubmit, reset } = methods

  const { joinChatRoom } = useChatRoom(setError)

  const onSubmit = () => {
    joinChatRoom(getValues('chat_room'))
  }

  const cancel = (e: FormEvent) => {
    e.preventDefault()
    reset({ chat_room: '' })
  }

  return (
    <Layout>
      <Container>
        <WhiteBox spacing="1.5rem">
          <Title>Join Chatroom</Title>
          <form style={{ width: '100%' }}>
            <SpacedColumn spacing="0.8rem">
              <FormProvider {...methods}>
                <JoinChatroomForm />
              </FormProvider>
              <SpacedRow align="center">
                <GreenButton onClick={handleSubmit(onSubmit)}>Join</GreenButton>
                <Button onClick={cancel}>Cancel</Button>
                {formState.isSubmitting && <Spinner />}
              </SpacedRow>
            </SpacedColumn>
          </form>
        </WhiteBox>
      </Container>
    </Layout>
  )
}

export default JoinChatroomPage
