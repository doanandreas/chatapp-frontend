import React, { ReactElement, useState, ChangeEvent } from 'react'

import { Center } from 'components/styled'
import RoomListObject from 'components/RoomListObject'
import styled from 'styled-components'

type RoomResponse = {
  id: string
  image_url: string
  name: string
  raw_id: string
  users_joined: number
}

type RoomListProps = {
  rooms: RoomResponse[]
}

const SearchBar = styled.input`
  width: 100%;
  padding: 10px 10px;
  border: none;
  color: ${({ theme }) => theme.colors.gray};
`

const RoomList = ({ rooms }: RoomListProps): ReactElement => {
  const [searchBar, setSearchBar] = useState('')

  const matchesSearchBar = (room: RoomResponse) =>
    room.name.toLowerCase().includes(searchBar)

  const roomIntoRoomListObject = (room: RoomResponse) => (
    <RoomListObject
      roomId={room.id}
      roomImageUrl={room.image_url}
      roomName={room.name}
      roomUsersJoined={room.users_joined}
      key={room.id}
    />
  )

  const handleSearchBarChange = (e: ChangeEvent<HTMLInputElement>) =>
    setSearchBar(e.target.value.toLowerCase())

  return (
    <Center>
      <SearchBar
        onChange={handleSearchBarChange}
        placeholder="Search for rooms..."
      />
      {rooms.filter(matchesSearchBar).map(roomIntoRoomListObject)}
    </Center>
  )
}

export default RoomList
