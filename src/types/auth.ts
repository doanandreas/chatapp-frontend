import {
  GoogleLoginResponse,
  GoogleLoginResponseOffline,
} from 'react-google-login'

export type User = {
  id: number
  email: string
  name: string
  family_name: string
  given_name: string
  image_url: string
}

export type IUserContext = {
  user: User
  isAuthenticated: boolean
  login: (res: GoogleLoginResponse | GoogleLoginResponseOffline) => void
  logout: VoidFunction
  userIsLoading: boolean
}
