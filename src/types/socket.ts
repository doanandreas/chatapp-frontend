import { Socket } from 'phoenix'
import { ReactElement } from 'react'
import { User } from './auth'

export type ISocketContext = {
  socket: Socket
}

export type SocketProviderProps = {
  options?: Record<string, string>
  children: ReactElement
}

// Channel Types
export type UseChatChannel = {
  messages: Messages
  broadcast: Broadcast
}

export type Messages = {
  [date: string]: MessageResponse[]
}

export type Broadcast = (event: string, payload: MessagePayload) => void

export type MessagePayload = {
  sender: number
  message: string
  chat_room: string
}

export type MessageResponse = {
  sender: User
  message: string
  inserted_at: string
  id: number
  read_by: number[]
}

export type ReadMessagePayload = {
  user_id: number
  message_id: number
}

export type ReadMessageResponse = {
  user_id: number
  message_id: number
  inserted_at: string
}
