import { ReactElement } from 'react'

type ChildrenProps = {
  children: ReactElement | ReactElement[]
}

type Json = Record<string, unknown>

export type { ChildrenProps, Json }
