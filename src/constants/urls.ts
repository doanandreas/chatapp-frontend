export {
  WEBSOCKET_URL,
  USER_URL,
  USER_BY_ID_URL,
  USER_JOIN_CHAT_ROOM_URL,
  CHAT_ROOM_URL,
  CHAT_ROOM_BY_ID_URL,
  CHAT_ROOM_BY_USER_ID_URL,
  MESSAGE_BY_ROOM_ID_URL,
}

const ALLOWED_HOST = process.env.REACT_APP_ALLOWED_HOST
const API_URL =
  process.env.NODE_ENV === 'production'
    ? `https://${ALLOWED_HOST}/api`
    : 'http://127.0.0.1:4000/api'
const WEBSOCKET_URL =
  process.env.NODE_ENV === 'production'
    ? `wss://${ALLOWED_HOST}/socket`
    : 'ws://127.0.0.1:4000/socket'

const USER_URL = `${API_URL}/users`
const USER_BY_ID_URL = (userId: string): string => `${USER_URL}/${userId}`

const USER_JOIN_CHAT_ROOM_URL = `${API_URL}/user_join_chat_rooms`

const CHAT_ROOM_URL = `${API_URL}/chat_rooms`
const CHAT_ROOM_BY_ID_URL = (roomId: string): string =>
  `${CHAT_ROOM_URL}/${roomId}`
const CHAT_ROOM_BY_USER_ID_URL = (userId: number): string =>
  `${API_URL}/get_chat_rooms_by_user_id/${userId}`

const MESSAGE_BY_ROOM_ID_URL = (roomId: string): string =>
  `${API_URL}/get_messages_by_chat_room_id/${roomId}`
