export { NEW_MESSAGE, READ_MESSAGE, NOTIFY_READ_MESSAGE }

const NEW_MESSAGE = 'new_msg'

const READ_MESSAGE = 'read_msg'
const NOTIFY_READ_MESSAGE = 'notify_read_msg'
