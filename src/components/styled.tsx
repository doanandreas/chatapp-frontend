import styled, { css } from 'styled-components'

export {
  Row,
  SpacedRow,
  Column,
  SpacedColumn,
  Center,
  Avatar,
  WhiteBox,
  Heading,
  Scrollbar,
}

const Flex = styled.div<{
  align?: string
  width?: string
}>`
  display: flex;
  align-items: ${({ align }) => (align ? align : 'flex-start')};
  width: ${({ width }) => (width ? width : '100%')};
`

const Row = styled(Flex)`
  flex-direction: row;
`

const SpacedRow = styled(Row)<{
  spacing?: string
}>`
  > *:not(style) ~ *:not(style) {
    margin-left: ${({ spacing }) => (spacing ? spacing : '0.5rem')};
    margin-top: 0;
  }
`

const Column = styled(Flex)`
  flex-direction: column;
`

const SpacedColumn = styled(Column)<{
  spacing?: string
}>`
  > *:not(style) ~ *:not(style) {
    margin-top: ${({ spacing }) => (spacing ? spacing : '0.5rem')};
    margin-left: 0;
  }
`

const Center = styled(Column)`
  justify-content: center;
  align-items: center;
  height: 100%;
`

const Avatar = styled.img<{
  size?: string
}>`
  width: ${({ size }) => (size ? size : '2.6rem')};
  height: ${({ size }) => (size ? size : '2.6rem')};
  border-radius: 50%;
`

const Scrollbar = css`
  margin: 0.2rem;
  scrollbar-color: ${({ theme }) => theme.colors.border} transparent;
  scrollbar-width: thin;

  &::-webkit-scrollbar {
    width: 5px;
  }
  &::-webkit-scrollbar-track {
    background-color: transparent;
  }
  &::-webkit-scrollbar-thumb {
    background-color: ${({ theme }) => theme.colors.border};
  }
`

const WhiteBox = styled(SpacedColumn)`
  align-items: center;
  background-color: white;
  width: 400px;
  border: 1px solid ${({ theme }) => theme.colors.border};
  border-radius: 0.2rem;
  padding: 2rem;

  @media (max-width: 768px) {
    width: 330px;
    padding: 2rem 1.5rem;
  }
  @media (max-width: 600px) {
    width: 90vw;
  }
`

const Heading = styled.p<{
  bolder?: boolean
}>`
  font-family: ${({ theme }) => theme.fonts.heading};
  font-size: 24px;
  font-weight: ${({ bolder }) => (bolder ? 600 : 300)};
  text-align: center;
`
