import React, { FormEvent, ReactElement } from 'react'
import styled from 'styled-components'
import theme from 'theme'

type ButtonProps = {
  onClick?: VoidFunction | ((e: FormEvent) => void)
  children: string
  disabled?: boolean
  bgColor?: string
  textColor?: string
  fullWidth?: boolean
}

const StyledButton = styled.button<{
  bgColor?: string
  fullWidth?: boolean
}>`
  background-color: ${({ bgColor }) => (bgColor ? bgColor : 'white')};
  border: 1px solid
    ${({ bgColor, theme }) => (bgColor ? bgColor : theme.colors.border)};
  border-radius: 0.2rem;
  text-align: center;
  width: ${({ fullWidth }) => (fullWidth ? '100%' : '4.6rem')};
  padding: 0.6rem 0.6rem 0.4rem;
  cursor: pointer;
`

const StyledText = styled.p<{
  textColor?: string
}>`
  color: ${({ textColor }) => (textColor ? textColor : 'black')};
  letter-spacing: 0.5px;
  font-size: 0.9rem;
`

const Button = ({
  onClick,
  children,
  disabled,
  bgColor,
  textColor,
  fullWidth,
}: ButtonProps): ReactElement =>
  fullWidth ? (
    <StyledButton
      onClick={onClick}
      disabled={disabled}
      bgColor={bgColor}
      fullWidth
    >
      <StyledText textColor={textColor}>{children}</StyledText>
    </StyledButton>
  ) : (
    <StyledButton onClick={onClick} disabled={disabled} bgColor={bgColor}>
      <StyledText textColor={textColor}>{children}</StyledText>
    </StyledButton>
  )

const GreenButton = (props: ButtonProps): ReactElement => (
  <Button
    bgColor={theme.colors.greenButton}
    textColor="white"
    {...props}
  ></Button>
)

export { Button, GreenButton }
