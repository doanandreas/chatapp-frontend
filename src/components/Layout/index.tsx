import React, { ReactElement } from 'react'
import styled from 'styled-components'

import { ChildrenProps } from 'types/general'
import { Center, Row } from 'components/styled'
import { useWindowSize } from 'hooks'
import { Sidebar } from 'containers'
import { TopBar } from './TopBar'

const WhiteContainer = styled.div`
  background-color: white;
  min-height: 100vh;
  width: 70vw;

  @media (max-width: 768px) {
    width: 100vw;
  }
`

const VSeparator = styled.div`
  background-color: ${({ theme }) => theme.colors.border};
  width: 1.5px;
  height: calc(100vh - 3rem);
`

const RightContainer = styled.div`
  width: 70%;
  height: calc(100vh - 3rem);

  @media (max-width: 768px) {
    width: 58%;
  }
`

const Layout = ({ children }: ChildrenProps): ReactElement => {
  const { isMobile } = useWindowSize()

  return (
    <Center>
      <WhiteContainer>
        <TopBar />
        {isMobile ? (
          children
        ) : (
          <Row>
            <Sidebar />
            <VSeparator />
            <RightContainer>{children}</RightContainer>
          </Row>
        )}
      </WhiteContainer>
    </Center>
  )
}

export default Layout
