import React, { ReactElement, useContext } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { UserContext } from 'store/UserContext'
import { Logo, Button } from 'components'
import { Row } from 'components/styled'

const TopRow = styled(Row)`
  justify-content: space-between;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.primary};
  padding: 0 1rem;
  height: 3rem;
`

const ButtonContainer = styled.div`
  display: flex;

  > * {
    margin-left: 10px;
  }
`

export const TopBar = (): ReactElement => {
  const { logout } = useContext(UserContext)

  return (
    <TopRow>
      <Logo width="5.5rem" />
      <ButtonContainer>
        <Link to="/room/join">
          <Button bgColor="#f5f5f5" fullWidth>
            Join room
          </Button>
        </Link>
        <Link to="/room/create">
          <Button bgColor="#f5f5f5" fullWidth>
            Create room
          </Button>
        </Link>
        <div>
          <Button bgColor="#f5f5f5" fullWidth onClick={logout}>
            Logout
          </Button>
        </div>
      </ButtonContainer>
    </TopRow>
  )
}
