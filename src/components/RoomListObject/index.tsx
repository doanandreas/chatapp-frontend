import React, { ReactElement, useState, useCallback, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'

import styled from 'styled-components'
import { Column, Row, Center } from 'components/styled'
import { request } from 'services/api'
import { MESSAGE_BY_ROOM_ID_URL } from 'constants/urls'
import { getDateTime } from 'utils/chat'

const Avatar = styled.img<{
  size?: string
}>`
  width: ${({ size }) => (size ? size : '52px')};
  height: ${({ size }) => (size ? size : '52px')};
  border-radius: 50%;
`

const RoomObjectContainer = styled(Center)`
  .room-obj {
    width: 100%;
    padding: 9px 17px;
    cursor: pointer;

    :hover {
      background-color: ${({ theme }) => theme.colors.grayChat};
    }

    .room-img {
      margin-right: 15px;
    }

    .room-info {
      width: 164px;
      height: 52px;
      justify-content: space-around;
      align-self: stretch;
    }

    .room-name {
      float: left;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      max-width: 130px;
      font-weight: 600;
      font-size: 13px;
    }

    .room-users-joined {
      margin-left: 4px;
      float: left;
      font-weight: 600;
      font-size: 13px;
    }

    .room-last-msg {
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 2; /* number of lines to show */
      -webkit-box-orient: vertical;
      font-weight: 300;
      font-size: 13px;
      color: ${({ theme }) => theme.colors.gray};
    }

    .room-msg-info {
      justify-self: flex-end;
      max-width: 60px;
    }

    .room-last-msg-time {
      float: right;
      margin-top: 4px;
      align-self: flex-end;
      font-weight: 300;
      font-size: 13px;
      color: ${({ theme }) => theme.colors.lightGray};
    }
  }
`

type Props = {
  roomId: string
  roomImageUrl: string
  roomName: string
  roomUsersJoined: number
}

const RoomListObject = ({
  roomId,
  roomImageUrl,
  roomName,
  roomUsersJoined,
}: Props): ReactElement => {
  const [lastMessage, setLastMessage] = useState('')
  const [lastMessageTime, setLastMessageTime] = useState('')
  const history = useHistory()
  const location = useLocation()

  const fetchChatData = useCallback(() => {
    request(MESSAGE_BY_ROOM_ID_URL(roomId))
      .then(({ data }) => {
        const lastMessageData = data[data.length - 1]
        const { time } = getDateTime(lastMessageData.inserted_at)
        setLastMessage(lastMessageData.message)
        setLastMessageTime(time)
      })
      .catch(response => console.log(response))
  }, [roomId])

  useEffect(() => {
    fetchChatData()
  }, [fetchChatData])

  const redirectToRoom = () => {
    history.push(`/chat/${roomId}/`)
  }

  return (
    <RoomObjectContainer>
      {location.pathname === `/chat/${roomId}/` ? (
        <div
          className="room-obj"
          style={{ backgroundColor: '#F5F5F5' }}
          key={roomId}
          onClick={redirectToRoom}
        >
          <Row>
            <Avatar className="room-img" src={roomImageUrl} />
            <Column className="room-info">
              <h3>
                <span className="room-name">{roomName}</span>
                <span className="room-users-joined">({roomUsersJoined})</span>
              </h3>
              <p className="room-last-msg">{lastMessage}</p>
            </Column>
            <Column className="room-msg-info">
              <p className="room-last-msg-time">{lastMessageTime}</p>
            </Column>
          </Row>
        </div>
      ) : (
        <div className="room-obj" key={roomId} onClick={redirectToRoom}>
          <Row>
            <Avatar className="room-img" src={roomImageUrl} />
            <Column className="room-info">
              <h3>
                <span className="room-name">{roomName}</span>
                <span className="room-users-joined">({roomUsersJoined})</span>
              </h3>
              <p className="room-last-msg">{lastMessage}</p>
            </Column>
            <Column className="room-msg-info">
              <p className="room-last-msg-time">{lastMessageTime}</p>
            </Column>
          </Row>
        </div>
      )}
    </RoomObjectContainer>
  )
}

export default RoomListObject
