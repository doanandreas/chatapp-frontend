import React, { ReactElement } from 'react'
import styled from 'styled-components'

import { SpacedRow, SpacedColumn, Avatar } from 'components/styled'
import { User } from 'types/auth'

type ReceivedMessageProps = {
  sender: User
  message: string
  inserted_at: string
}

type SentMessageProps = {
  message: string
  inserted_at: string
  read_by: number[]
}

const FlexEndContainer = styled(SpacedRow)<{
  width?: string
}>`
  justify-content: flex-end;
  align-items: flex-end;
  width: ${({ width }) => width};
`

const MessageBox = styled.div<{
  isSent?: boolean
}>`
  background-color: ${({ theme, isSent }) =>
    isSent ? theme.colors.greenChat : theme.colors.grayChat};
  border-radius: 0.7rem;
  padding: 0.6rem 0.9rem;
  font-size: 0.9rem;
  max-width: 90%;
  word-wrap: break-word;
`

const SenderName = styled.p`
  font-size: 0.9rem;
`

const SmallInfoText = styled.p`
  font-size: 0.75rem;
  color: ${({ theme }) => theme.colors.lightGray};
`

const ReceivedMessage = ({
  sender,
  message,
  inserted_at,
}: ReceivedMessageProps): ReactElement => (
  <SpacedRow>
    <Avatar src={sender.image_url} />
    <SpacedColumn spacing="0.4rem" width="100%">
      <SenderName>{sender.name}</SenderName>
      <SpacedRow align="flex-end" width="calc(90% - 4rem)">
        <MessageBox>{message}</MessageBox>
        <SmallInfoText>{inserted_at}</SmallInfoText>
      </SpacedRow>
    </SpacedColumn>
  </SpacedRow>
)

const SentMessage = ({
  message,
  inserted_at,
  read_by,
}: SentMessageProps): ReactElement => (
  <FlexEndContainer>
    <FlexEndContainer width="90%">
      <div>
        {read_by && read_by.length > 1 && (
          <SmallInfoText style={{ textAlign: 'right' }}>
            Read {read_by.length - 1}
          </SmallInfoText>
        )}
        <SmallInfoText style={{ textAlign: 'right' }}>
          {inserted_at}
        </SmallInfoText>
      </div>
      <MessageBox isSent>{message}</MessageBox>
    </FlexEndContainer>
  </FlexEndContainer>
)

export { ReceivedMessage, SentMessage }
