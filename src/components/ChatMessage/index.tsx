import React, { ReactElement, useContext } from 'react'

import { UserContext } from 'store/UserContext'
import { MessageResponse } from 'types/socket'
import { ReceivedMessage, SentMessage } from './Message'

const ChatMessage = ({ sender, ...rest }: MessageResponse): ReactElement => {
  const { user } = useContext(UserContext)

  return sender.id === user.id ? (
    <SentMessage {...rest} />
  ) : (
    <ReceivedMessage sender={sender} {...rest} />
  )
}

export default ChatMessage
