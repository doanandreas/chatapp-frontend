import React, { ReactElement } from 'react'
import { useFormContext } from 'react-hook-form'
import styled from 'styled-components'

import { SpacedColumn } from 'components/styled'

type InputProps = {
  name: string
  label: string
  placeholder?: string
  isRequired?: boolean
  defaultErrMessage?: string
  errMessage?: string
}

const StyledLabel = styled.label`
  font-size: 0.9rem;
  color: ${({ theme }) => theme.colors.primary};
`

const StyledInput = styled.input<{
  error?: boolean
}>`
  background-color: white;
  border: 1.5px solid ${({ theme, error }) =>
    error ? 'red' : theme.colors.border};
  border-radius: 0.2rem;
  width: 100%;
  font-size: 0.9rem;
  letter-spacing: 0.5px;
  padding: 0.6rem 0.9rem;

  ::placeholder
    color: ${({ theme }) => theme.colors.lightGray};
  }
`

const ErrorMessage = styled.p`
  color: red;
  font-size: 0.9rem;
`

const Input = ({
  name,
  label,
  placeholder,
  isRequired,
  defaultErrMessage,
}: InputProps): ReactElement => {
  const { register, errors } = useFormContext()
  const error = errors[name]

  return (
    <SpacedColumn align="flex-start" spacing="0.3rem">
      <StyledLabel>
        {label} {isRequired && <span style={{ color: 'red' }}>*</span>}{' '}
      </StyledLabel>
      <StyledInput
        ref={register({
          required: isRequired && defaultErrMessage,
        })}
        name={name}
        placeholder={placeholder}
        error={error}
      />
      {error && <ErrorMessage>{error.message}</ErrorMessage>}
    </SpacedColumn>
  )
}

export default Input
