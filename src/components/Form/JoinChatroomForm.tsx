import React, { ReactElement } from 'react'
import { Input } from 'components/Form/Fields'

const JoinChatroomForm = (): ReactElement => (
  <Input
    name="chat_room"
    label="Room ID"
    placeholder="Enter room ID"
    defaultErrMessage="Please enter a room ID first!"
    isRequired
  />
)

export default JoinChatroomForm
