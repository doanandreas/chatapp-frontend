import React, { ReactElement } from 'react'
import { Input } from 'components/Form/Fields'

const CreateChatroomForm = (): ReactElement => (
  <>
    <Input name="name" label="Room name" placeholder="Pemfung group" />
    <Input
      name="id"
      label="Room ID"
      placeholder="pemfung12"
      defaultErrMessage="Please enter a room ID."
      isRequired
    />
  </>
)

export default CreateChatroomForm
