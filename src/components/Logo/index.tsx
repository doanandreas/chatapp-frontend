import React, { ReactElement } from 'react'
import { Link } from 'react-router-dom'

import { Chat, PhoenixLogo, ReactLogo } from 'assets'
import { SpacedRow } from 'components/styled'

type LogoProps = {
  width?: string
}

const Logo = ({ width }: LogoProps): ReactElement => (
  <Link to="/">
    <SpacedRow spacing="0.2rem" width={width} align="center">
      <ReactLogo />
      <PhoenixLogo />
      <Chat />
    </SpacedRow>
  </Link>
)

export default Logo
