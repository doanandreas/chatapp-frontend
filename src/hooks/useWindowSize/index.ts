import { useState, useEffect } from 'react'
import { MOBILE_MAX_WIDTH } from 'constants/size'

type UseWindowSize = {
  width: number
  height: number
  isMobile: boolean
}

const useWindowSize = (): UseWindowSize => {
  const [width, setWidth] = useState(window.innerWidth)
  const [height, setHeight] = useState(window.innerHeight)

  useEffect(() => {
    const handleResize = () => {
      setWidth(window.innerWidth)
      setHeight(window.innerHeight)
    }

    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [])

  return {
    width,
    height,
    isMobile: width <= MOBILE_MAX_WIDTH,
  }
}

export default useWindowSize
