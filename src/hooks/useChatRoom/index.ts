import { useContext } from 'react'
import { useHistory } from 'react-router-dom'
import { ErrorOption } from 'react-hook-form/dist/types'

import { USER_JOIN_CHAT_ROOM_URL, CHAT_ROOM_URL } from 'constants/urls'
import { request } from 'services/api'
import { UserContext } from 'store/UserContext'

type setErrorFunction = (name: string, error: ErrorOption) => void

type createChatRoomProps = {
  id: string
  name?: string
}

type UseChatRoom = {
  joinChatRoom: (chat_room: string) => void
  createChatRoom: ({ id, name }: createChatRoomProps) => void
}

const useChatRoom = (setError: setErrorFunction): UseChatRoom => {
  const { user } = useContext(UserContext)
  const history = useHistory()

  const joinChatRoom = (chat_room: string) => {
    const redirectToChatRoom = () => {
      history.push(`/chat/${chat_room}/`)
    }

    const payload = {
      user_join_chat_room: {
        user: user.id,
        chat_room,
      },
    }
    request(USER_JOIN_CHAT_ROOM_URL, payload, 'POST')
      .then(redirectToChatRoom)
      .catch(({ error }) => {
        error === 'User already joined'
          ? redirectToChatRoom()
          : handleRoomDoesNotExist()
      })
  }

  const createChatRoom = (props: createChatRoomProps) => {
    const payload = {
      chat_room: props,
    }
    request(CHAT_ROOM_URL, payload, 'POST')
      .then(({ data }) => {
        joinChatRoom(data.id)
      })
      .catch(handleRoomExists)
  }

  const handleRoomDoesNotExist = () => {
    setError('chat_room', {
      type: 'manual',
      message: 'Room with given ID does not exist.',
    })
  }

  const handleRoomExists = () => {
    setError('id', {
      type: 'manual',
      message: 'Room with given ID exists.',
    })
  }

  return {
    joinChatRoom,
    createChatRoom,
  }
}

export default useChatRoom
