import { useContext, useEffect, useState, useCallback } from 'react'

import {
  NEW_MESSAGE,
  READ_MESSAGE,
  NOTIFY_READ_MESSAGE,
} from 'constants/action'
import { SocketContext } from 'store/SocketContext'
import { UserContext } from 'store/UserContext'
import {
  Broadcast,
  ReadMessageResponse,
  MessagePayload,
  MessageResponse,
  UseChatChannel,
  Messages,
} from 'types/socket'
import { getDateTime } from 'utils/chat'

const failedToJoinWarning = () => console.error('Failed to join channel.')

const useChatChannel = (channelName: string): UseChatChannel => {
  const { socket } = useContext(SocketContext)
  const { user } = useContext(UserContext)

  const [messages, setMessages] = useState<Messages>({})
  const [broadcast, setBroadcast] = useState<Broadcast>(failedToJoinWarning)

  const addMessageBasedOnDate = (payload: MessageResponse) => {
    const { date, time } = getDateTime(payload.inserted_at)
    return function (msgs: Messages) {
      if (!msgs[date]) {
        msgs[date] = []
      }
      msgs[date].push({ ...payload, inserted_at: time })
      return msgs
    }
  }

  function addReaderInMessage(user_id: number, message_id: number) {
    return function (msg: MessageResponse) {
      const isTargetMessage = msg.id === message_id
      const hasBeenRead = msg.read_by.some(id => id === user_id)

      return isTargetMessage && !hasBeenRead
        ? { ...msg, read_by: [...msg.read_by, user_id] }
        : msg
    }
  }

  const updateReadMessage = useCallback(
    ({ user_id, message_id, inserted_at }: ReadMessageResponse) => {
      const { date } = getDateTime(inserted_at)
      const updateReadBy = addReaderInMessage(user_id, message_id)
      setMessages(msgs => {
        const newMessages = { ...msgs }
        newMessages[date] = msgs[date].map(updateReadBy)
        return newMessages
      })
    },
    [setMessages],
  )

  useEffect(() => {
    const channel = socket.channel(channelName, { user: user.id })

    const handleReceiveNewMessage = (payload: MessageResponse) => {
      const updatedMessages = addMessageBasedOnDate(payload)
      setMessages(updatedMessages)
      channel.push(READ_MESSAGE, {
        user_id: user.id,
        message_id: payload.id,
      })
    }

    channel
      .join()
      .receive('ok', () => {
        console.log('Successfully joined channel.')
        setMessages({ '': [] })
      })
      .receive('error', failedToJoinWarning)

    channel.on(NEW_MESSAGE, handleReceiveNewMessage)
    channel.on(NOTIFY_READ_MESSAGE, updateReadMessage)

    setBroadcast(() => (event: string, payload: MessagePayload) =>
      channel.push(event, payload),
    )

    return () => {
      channel.leave()
    }
  }, [channelName, socket, updateReadMessage, user.id])

  return {
    messages,
    broadcast,
  }
}

export default useChatChannel
