import { DefaultTheme } from 'styled-components'

const fonts = {
  heading: 'Segoe UI, sans-serif',
  body: 'system-ui, sans-serif',
}

const colors = {
  body: '#2C3E50',
  primary: '#2A334A',
  lightGray: '#B7B7B7',
  gray: '#777',
  grayChat: '#F5F5F5',
  greenChat: '#C3F69D',
  greenButton: '#07B53B',
  border: '#DADEE1',
}

const theme: DefaultTheme = { colors, fonts }

export default theme
