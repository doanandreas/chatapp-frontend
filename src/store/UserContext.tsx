import React, {
  createContext,
  useCallback,
  useEffect,
  useState,
  ReactElement,
} from 'react'
import {
  GoogleLoginResponse,
  GoogleLoginResponseOffline,
} from 'react-google-login'

import { AUTH_STORAGE_KEY } from 'constants/auth'
import { IUserContext, User } from 'types/auth'
import { ChildrenProps } from 'types/general'
import { request } from 'services/api'
import { USER_URL, USER_BY_ID_URL } from 'constants/urls'

const initialUser = {
  id: 0,
  email: '',
  name: '',
  family_name: '',
  given_name: '',
  image_url: '',
}

export const UserContext = createContext<IUserContext>({
  user: initialUser,
  isAuthenticated: false,
  login: () => null,
  logout: () => null,
  userIsLoading: true,
})

export const UserProvider = ({ children }: ChildrenProps): ReactElement => {
  const [user, setUser] = useState<User>(initialUser)
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false)
  const [userIsLoading, setUserIsLoading] = useState<boolean>(true)

  const getUser = useCallback(async () => {
    setUserIsLoading(true)
    const userId = localStorage.getItem(AUTH_STORAGE_KEY)
    if (userId) {
      await request(USER_BY_ID_URL(userId)).then(saveUser).catch(console.error)
    }
    setUserIsLoading(false)
  }, [])

  const login = (res: GoogleLoginResponse | GoogleLoginResponseOffline) => {
    if ('profileObj' in res) {
      const user = res.profileObj
      const payload = {
        user: {
          email: user.email,
          name: user.name,
          family_name: user.familyName,
          given_name: user.givenName,
          image_url: user.imageUrl,
        },
      }
      request(USER_URL, payload, 'POST')
        .then(({ data }) => {
          saveUser({ data })
          localStorage.setItem(AUTH_STORAGE_KEY, data.id)
        })
        .catch(console.log)
    }
  }

  const logout = () => {
    localStorage.removeItem(AUTH_STORAGE_KEY)
    setUser(initialUser)
    setIsAuthenticated(false)
  }

  const saveUser = ({ data }: { data: User }) => {
    setUser(data)
    setIsAuthenticated(true)
  }

  useEffect(() => {
    getUser()
  }, [getUser])

  return (
    <UserContext.Provider
      value={{ user, isAuthenticated, login, logout, userIsLoading }}
    >
      {children}
    </UserContext.Provider>
  )
}
