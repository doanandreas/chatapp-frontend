import React, { useEffect, useMemo, createContext, ReactElement } from 'react'
import { Socket } from 'phoenix'

import { WEBSOCKET_URL } from 'constants/urls'
import { ISocketContext, SocketProviderProps } from 'types/socket'

export const SocketContext = createContext<ISocketContext>({
  socket: new Socket(WEBSOCKET_URL),
})

export const SocketProvider = ({
  options = {},
  children,
}: SocketProviderProps): ReactElement => {
  const socket = useMemo(() => new Socket(WEBSOCKET_URL, { params: options }), [
    options,
  ])

  useEffect(() => {
    socket.connect()

    return () => {
      socket.disconnect()
    }
  }, [socket])

  return (
    <SocketContext.Provider value={{ socket }}>
      {children}
    </SocketContext.Provider>
  )
}
