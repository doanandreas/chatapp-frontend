export { getDateTime }

type GetDateTime = {
  date: string
  time: string
}

const getDateTime = (dateString: string): GetDateTime => {
  const date = new Date(`${dateString}Z`)
  const splitDate = date.toString().split(' ')
  const time = splitDate[4].split(':')

  return {
    date: `${splitDate[0]}, ${splitDate.slice(1, 4).join(' ')}`,
    time: `${time[0]}:${time[1]}`,
  }
}
