# ChatApp Front End (CAFE)

## About ChatApp

ChatApp is a web-based application for multi-person real-time chatting with Google authentication and read by feature. Check out our ChatApp [here](https://chatapp-pemfung.netlify.app)!

## Functional Programming Concepts on CAFE

CAFE serves as the frontend service of ChatApp, built based on functional programming with React library and Typescript language. Other than React's built-in declarative features (function components and hooks), CAFE also applied functional programming concepts, which include the following:

1. Pure Function (ex: [ChatMessage](https://gitlab.cs.ui.ac.id/kelompok-pemfung/cafe/-/blob/master/src/components/ChatMessage/Message.tsx#L48) components)
2. Partial Evaluation (ex: [updateReadBy](https://gitlab.cs.ui.ac.id/kelompok-pemfung/cafe/-/blob/master/src/hooks/useChatChannel/index.ts#L54) function)
3. Pattern Matching (ex: [Phoenix socket channel's on](https://gitlab.cs.ui.ac.id/kelompok-pemfung/cafe/-/blob/master/src/hooks/useChatChannel/index.ts#L84) function)
4. Functional Data Structure (ex: [ProtectedRoute](https://gitlab.cs.ui.ac.id/kelompok-pemfung/cafe/-/blob/master/src/routes/ProtectedRoute.tsx#L19) component)
5. Higher Order Function (ex: [map](https://gitlab.cs.ui.ac.id/kelompok-pemfung/cafe/-/blob/master/src/hooks/useChatChannel/index.ts#L57) function)
6. Abstraction and Functional Modularity (ex: [useChatRoom](https://gitlab.cs.ui.ac.id/kelompok-pemfung/cafe/-/blob/master/src/hooks/useChatRoom/index.ts#L21) hooks)

## Installation

* Install npm (version 6 or later).
* Clone the repository.
* Enter the project directory with `cd cafe`.
* Build the dependencies with `npm install` or `npm i`.
* Create `.env` file based on [.env.example](https://gitlab.cs.ui.ac.id/kelompok-pemfung/cafe/-/blob/master/.env.example) and fill in the Google Client ID.
* Run the app in development node with `npm start`.
* Run ChatApp backend service from [ChatApp repository](https://gitlab.cs.ui.ac.id/kelompok-pemfung/chatapp).
* Open [http://localhost:3000](http://localhost:3000) to view it in the browser and see ChatApp in action!

For production purposes, see the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Authors [Kelompok 18]:
* [Doan Andreas Nathanael](https://gitlab.cs.ui.ac.id/doanandreas18)
* [Glenda Emanuella Sutanto](https://gitlab.cs.ui.ac.id/glendaesutanto)
* [Kefas Satrio Bangkit S.](https://gitlab.cs.ui.ac.id/kefas.satrio)
* [Wulan Mantiri](https://gitlab.cs.ui.ac.id/wulanmantiri)

## Acknowledgements
* CS UI - Functional Programming 2020
